# Banco de dados (Migrations)

## Configurando tabela de User
```
      table.increments() //Define nossa chave primária
      table.string('name', 80)
      table.string('surname', 200)
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.integer('image_id').unsigned() //Inteiro não negativo
      table.timestamps()
```

## Criando tabela de Image
- Criando a migration para tabela Image ```adonis make:migration Image```.

```
      table.string('path', 256) //Nome do arquivo no disco local
      table.integer('size').unsiged() //Inteiro positivo
      table.string('original_name', 100)
      table.string('extension', 10)
```

## Criando o relacionamento entre User e Image
- Criando migration para tabela de relacionamento UserImage ```adonis make:migration UserImageFk``` (Foreign key) do tipo **select**.
- Trocar nome **user_image_fks** por **users**, pois está fazendo um select na tabela de usuários.

Criando relacionamento (Foreign key):
```
    table.foreign('image_id').references('id').inTable('images').onDelete('cascade')
```


**Dropando foreign key:**
```
table.dropForeign('image_id')
```

## Criando migration de Category
- Criando a migration para tabela Category ```adonis make:migration Category```.

## Criando migration de Product
- Criando a migration para tabela Category ```adonis make:migration Product```.

## Criando migration de Coupon
- Criando a migration para tabela Coupon ```adonis make:migration Coupon```.

## Criando migration de Order
- Criando a migration para tabela Coupon ```adonis make:migration Order```.

## Criando o relacionamento entre Coupon e Order
- Criando tabela de relacionamento (pivo) ```adonis make:migration CouponOrder```.
- Após criar, trocar nome de plural para singular **coupon_order**, pois é uma tabela pivo.

## Criando migration de OrderItem
- Criando a migration para tabela Coupon ```adonis make:migration OderItem```.