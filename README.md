# Ecommerce com AdonisJS, Node.js e MySQL
AdonisJS é um framework Node baseado na arquitetura MVC e voltado para aplicações monoliticas.

## Instalação do AdonisJS:
- AdonisJS CLI ```npm i -g @adonisjs/cli```.

## Comandos CLI AdonisJS:
- Criar um novo projeto ```adonis new <nome do projeto> --api-only```.
- Iniciar servidor do projeto ```adonis serve --dev```.

## Configuração do banco:
No arquivo ```.env``` ficam todas as variaveis de ambiente usadas na configuração do banco de dados.

Após configurar as variaveis de ambiente, o driver do banco de dados deve ser instalado, no caso o do MySQL. ```npm i --save mysql```.

## Criando arquivos de LOGs:
Toda vez que um erro ocorrer na aplicação, uma notificação de log será gerada em um arquivo.
Isso será feito por meio das Exceptions Handling, que interrompem o programa caso algo não esteja funcionando corretamente.

- Criando uma Exception Handling ```adonis make:ehandler```.
    Caso não tenha nenhuma Exception ainda, uma pasta será criada em App chamada Exceptions.
- Criar uma constante ```const Logger = use('Logger')```.
- Ir no método de report e criar a configuração de LOG:
```
    if (error.status >= 500) {
      Logger.erro(error.message, {
        stack: erro.stack,
        message: error.message,
        name: error.name
      })
    }
```
- A LOG pode ser exibida por duas formas de transporte, via console ou arquivo. Isso pode ser alterado em ```Config/App``` nas configurações de Logger.

## Instalando pacotes no AdonisJS:
Quando um pacote é instalado no Adonis, ele executa algumas ações após o término da instalação.

- Instalando pacote para **envio de e-mails** no Adonis ```adonis install @adonisjs/mail```.
  - Após a instalação, é necessário colar o provider dentro da pasta **start/app.js**
    Provider: ```'@adonisjs/mail/providers/MailProvider'```


- Instalando pacote **validator** no Adonis ```adonis install @adonisjs/validator ```.
  - Após a instalação, é necessário colar o provider dentro da pasta **start/app.js**
    Provider: ```'@adonisjs/validator/providers/ValidatorProvider'```
  
  
- Instalando pacote **websocket** no Adonis ```adonis install @adonisjs/websocket```.
  - Após a instalação, é necessário colar o provider dentro da pasta **start/app.js**
    Provider: ```'@adonisjs/websocket/providers/WsProvider'```

- Instalando pacote **acl** no Adonis ```adonis install adonisjs-acl```.
  - Após a instalação, é necessário colar o provider dentro da pasta **start/app.js**
    Provider: ```'adonis-acl/providers/AclProvider'```
    AceProveider: ```'adonis-acl/providers/CommandsProvider'``` (Providers que irão rodar em linha de comando).
    
**Obs: acl não abre um link de instalação de providers, os mesmos podem ser consultados na página do npm.**

  - Criar aliases do acl no objeto aliases:
    ```
    const aliases = {
        ...
        Role: 'Adonis/Acl/Role',
        Permission: 'Adonis/Acl/Permission',
        ...
    }
    ```

  - Registrar traits no model Users depois da função **boot()**. Isso vai indicar que o model User tem regra e permissões.
    ```
    static get traits () {
        return [
        '@provider:Adonis/Acl/HasRole',
        '@provider:Adonis/Acl/HasPermission'
        ]
    }
    ```

    - Finalizando a instalação do pacote criando as migrations: ```adonis acl:setup```.

- Instalando pacote **bumblebee** no Adonis ```adonis install adonis-bumblebee```.
  - Após a instalação, é necessário colar o provider dentro da pasta **start/app.js**
    Provider: ```'@adonisjs/validator/providers/ValidatorProvider'```